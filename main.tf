provider "aws"{
    region = "us-east-1"
}

variable "subnet_cidr_block" {
    description = "subnet cidr block"
}

variable "vpc_cidr_block" {
    description = "vpc cidr block"
}

resource "aws_vpc" "vpc_dev"{
    cidr_block = var.vpc_cidr_block  
    tags = {
        Name: "development"
    } 
}

resource "aws_subnet" "vpc_subnet"{
    vpc_id = aws_vpc.vpc_dev.id
    cidr_block = var.subnet_cidr_block
    availability_zone = "us-east-1a"
    tags = {
        Name: "subnet-1-dev"
    } 
}

output "vpc_id" {
  value = aws_vpc.vpc_dev.id
}